# jsonata-rust

A Rust implementation of the Jsonata query tool. https://jsonata.org/ 

The goal of this project is to implement a number of jsonata features in Rust which
is a fairly popular tool used in Node and Javascript applications. Jsonata has a lot
of features including functional programming features and includes a long set of convenience features.

Due to the time constraints, I don't think I'll be able to implement the entire feature set, but perhaps
just implement a core set of features.

## Jsonata Feature Goals
- [x] Simple Queries
- [x] JSON Arrays
- [x] Predicate Queries
- [x] Functions and Expressions (All expressions must be wrapped in parentheses)

## Expression types supported
`String`, `uint`, `bool`.

## Expression operations supported
`&`, `+`, `-`, `*`, `/`, `%`, `and`, `or`

## Stretch Goals
There are many, many more features included in Jsonata. I'll include them as a stretch goal
just in case the above are too easy ;P. 
- [x] Sorting, Grouping, Aggregation (Added "Order By" Functionality)
- [ ] Functional Programming (I actually don't like this feature, seems kind of pointless)
- [ ] Regular Expressions
- [ ] Date/Time processing

## Development Notes

Jsonata was originally designed for javascript. Javascript is a dynamically typed language
and so the Jsonata language was designed with this aspect in mind. In dynamically typed languages,
it is easy enough to, for example, add together an int and a float. But in Rust, the language
usually demands that the user specifically type each kind of number before performing operations.
It's not possible to make all of these decisions in Rust, so some ideas in Jsonata simply do not
port well to Rust. 

However, the core elements of the Jsonata language work just fine. I've implemented most of the core aspects
of Jsonata that allow the user to easily pull deeply nested data out of the object: the main perk of using
a library like this. The rest of the library are convenience functions: sorting, filtering, etc.