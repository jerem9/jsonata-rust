expr = { keys | "(" ~ val_expr ~ ")" }
keys = _{ key ~ ("." ~ key)* ~ (".(" ~ val_expr ~ ")")? ~ ("^(" ~ orderby ~ ")")? }
root = { "$" }
key = _{ (key_plain | key_literal | root) ~
	("[" ~ (index | predicate) ~"]")? }
key_plain = @{ ASCII_ALPHANUMERIC+ }
// Only way to get leading whitespace working.
// Causes an unnecessary nesting, but will have to wait for a
// silent atomic rule to fix it.
key_literal = ${ "`" ~ char ~ "`" }
char = @{(
    !("`" | "\\") ~ ANY
    | "\\" ~ ("\"" | "\\" | "/" | "b" | "f" | "n" | "r" | "t")
    | "\\" ~ ("u" ~ ASCII_HEX_DIGIT{4})
)+}
index = @{ ASCII_DIGIT+ }
predicate = { key_plain ~ "=" ~ "'" ~ key_plain ~ "'" }

val_expr = { operand ~ (op ~ operand)* }
operand = _{ (bool | integer | key | "(" ~ val_expr ~ ")") | string }
string = ${ "'" ~  string_chars  ~ "'" }
string_chars = @{(
    !("'" | "\\") ~ ANY
	| "\\" ~ ("\'" | "\\" | "/" | "b" | "f" | "n" | "r" | "t")
    | "\\" ~ ("u" ~ ASCII_HEX_DIGIT{4})
)*}
integer = { ASCII_DIGIT+ }
bool = { "true" | "false" }
op = { concat | add | sub | mul | div | pest_mod | and | or }
concat = { "&" }
add = { "+" }
sub = { "-" }
mul = { "*" }
div = { "/" }
pest_mod = { "%" }
and = { "and" }
or = { "or" }

orderby = { ("<"? ~ ascending_orderby) | (">" ~ descending_orderby) }
ascending_orderby = { ASCII_ALPHANUMERIC+ }
descending_orderby = { ASCII_ALPHANUMERIC+ }
WHITESPACE = _{ " " }