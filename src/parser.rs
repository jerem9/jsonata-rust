use pest::error::Error;
use pest::iterators::Pair;
use pest::Parser;
use std::cmp::Ordering;
use std::str::FromStr;

#[derive(Parser)]
#[grammar = "jsonata.pest"]
pub struct JsonataParser;

type JsonataValue = Option<serde_json::Value>;
type Ast = Vec<AstNode>;

// Stubbed for future unary operators, none added yet.
pub enum UnaryOp {}

pub enum BinaryOp {
    Concat,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    And,
    Or,
}

pub enum AstNode {
    Key(String),
    KeyStr(String),
    Str(String),
    Int(usize),
    Bool(bool),
    Index(usize),
    OrderBy(String, bool),
    UnaryOp {
        op: UnaryOp,
        expr: Box<AstNode>,
    },
    BinaryNode {
        op: BinaryOp,
        lhs: Box<AstNode>,
        rhs: Box<AstNode>,
    },
    Predicate {
        lhs: Box<AstNode>,
        rhs: Box<AstNode>,
    },
}

pub fn parse_jsonata(query: &str) -> Result<Ast, Error<Rule>> {
    let parsed_query = JsonataParser::parse(Rule::expr, query)?.next().unwrap();
    parse_jsonata_to_ast(parsed_query)
}

fn parse_jsonata_to_ast(query: pest::iterators::Pair<Rule>) -> Result<Ast, Error<Rule>> {
    let mut ast = vec![];
    for pair in query.into_inner() {
        match pair.as_rule() {
            Rule::root => {}
            Rule::key_plain => ast.push(AstNode::Key(pair.as_str().to_string())),
            Rule::key_literal => ast.push(AstNode::Key(
                pair.into_inner().next().unwrap().as_str().to_string(),
            )),
            Rule::predicate => {
                let mut vals = pair.into_inner();
                let lhs = vals.next().unwrap().as_str().to_string();
                let rhs = vals.next().unwrap().as_str().to_string();
                ast.push(AstNode::Predicate {
                    lhs: Box::new(AstNode::Key(lhs)),
                    rhs: Box::new(AstNode::Str(rhs)),
                });
            }
            Rule::index => {
                ast.push(AstNode::Index(pair.as_str().parse::<usize>().unwrap()));
            }
            Rule::orderby => {
                let is_ascending = pair.into_inner().next().unwrap();
                match is_ascending.as_rule() {
                    Rule::ascending_orderby => {
                        ast.push(AstNode::OrderBy(is_ascending.as_str().to_string(), true))
                    }
                    Rule::descending_orderby => {
                        ast.push(AstNode::OrderBy(is_ascending.as_str().to_string(), false))
                    }
                    _ => panic!("Orderby cannot take rule: {}", is_ascending),
                }
            }
            Rule::val_expr => ast.push(parse_val_expr(pair)),
            _ => panic!("Hit a rule that shouldn't exist: {}", pair.as_str()),
        }
    }

    Ok(ast)
}

fn parse_val_expr(pair: Pair<Rule>) -> AstNode {
    let mut expr = pair.into_inner();
    let operand = expr.next().unwrap();
    let mut root = parse_val_operand(operand);
    while let Some(op) = expr.next() {
        root = AstNode::BinaryNode {
            op: parse_op(op.into_inner().next().unwrap()),
            lhs: Box::new(root),
            rhs: Box::new(parse_val_operand(expr.next().unwrap())),
        }
    }
    root
}

fn parse_val_operand(pair: Pair<Rule>) -> AstNode {
    match pair.as_rule() {
        Rule::val_expr => parse_val_expr(pair),
        Rule::key_plain => AstNode::KeyStr(pair.as_str().to_string()),
        Rule::key_literal => {
            AstNode::KeyStr(pair.into_inner().next().unwrap().as_str().to_string())
        }
        Rule::string => AstNode::Str(pair.into_inner().next().unwrap().as_str().to_string()),
        Rule::integer => AstNode::Int(pair.as_str().parse().unwrap()),
        Rule::bool => AstNode::Bool(FromStr::from_str(pair.as_str()).unwrap()),
        _ => panic!("Incorrect value in value expression: {}", pair.as_str()),
    }
}

fn parse_op(pair: Pair<Rule>) -> BinaryOp {
    match pair.as_rule() {
        Rule::concat => BinaryOp::Concat,
        Rule::add => BinaryOp::Add,
        Rule::sub => BinaryOp::Sub,
        Rule::mul => BinaryOp::Mul,
        Rule::div => BinaryOp::Div,
        Rule::pest_mod => BinaryOp::Mod,
        Rule::and => BinaryOp::And,
        Rule::or => BinaryOp::Or,
        _ => panic!("Invalid op found: {}", pair.as_str()),
    }
}

pub fn parse_json(ast: &Ast, json_str: &str) -> JsonataValue {
    let json: serde_json::Value = serde_json::from_str(json_str).unwrap();
    ast.iter().fold(Some(json), |j, n| match j {
        Some(_) => eval_node(n, &j.unwrap()),
        _ => None,
    })
}

fn eval_node(node: &AstNode, json: &serde_json::Value) -> JsonataValue {
    match node {
        AstNode::Str(s) => Some(serde_json::Value::String(s.to_string())),
        AstNode::Int(i) => Some(serde_json::Value::Number(serde_json::Number::from(*i))),
        AstNode::Bool(b) => Some(serde_json::Value::Bool(*b)),
        AstNode::Key(s) => key_lookup(s, json),
        AstNode::KeyStr(s) => match key_lookup(s, json) {
            Some(x) => Some(x),
            None => Some(serde_json::Value::String(s.to_string())),
        },
        AstNode::Index(i) => index_lookup(*i, json),
        AstNode::OrderBy(s, is_ascending) => orderby(s, *is_ascending, json),
        AstNode::Predicate { lhs, rhs } => {
            if let AstNode::Key(key) = &**lhs {
                if let AstNode::Str(val) = &**rhs {
                    predicate_filter(key, val, json)
                } else {
                    None
                }
            } else {
                None
            }
        }
        AstNode::UnaryOp { op, expr } => eval_node(expr, json).map(|val| eval_unary(op, val)),
        AstNode::BinaryNode { op, lhs, rhs } => {
            let l = eval_node(lhs, json);
            let r = eval_node(rhs, json);
            match (l, r) {
                (Some(l), Some(r)) => Some(eval_binary(op, l, r)),
                _ => None,
            }
        }
    }
}

// Stubbed for future unary operations, but none created yet.
fn eval_unary(_: &UnaryOp, exp: serde_json::Value) -> serde_json::Value {
    exp
}

fn eval_binary(
    op: &BinaryOp,
    lexp: serde_json::Value,
    rexp: serde_json::Value,
) -> serde_json::Value {
    match op {
        BinaryOp::Concat => concat(lexp, rexp),
        BinaryOp::Add => add(lexp, rexp),
        BinaryOp::Sub => sub(lexp, rexp),
        BinaryOp::Mul => mul(lexp, rexp),
        BinaryOp::Div => div(lexp, rexp),
        BinaryOp::Mod => r#mod(lexp, rexp),
        BinaryOp::And => and(lexp, rexp),
        BinaryOp::Or => or(lexp, rexp),
    }
}

fn concat(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    let s1 = coerce_to_string(lexp);
    let s2 = coerce_to_string(rexp);
    serde_json::Value::String(s1 + &s2)
}

fn add(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    number_op(lexp, rexp, |x, y| x + y)
}

fn sub(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    number_op(lexp, rexp, |x, y| x - y)
}

fn mul(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    number_op(lexp, rexp, |x, y| x * y)
}

fn div(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    number_op(lexp, rexp, |x, y| x / y)
}

fn r#mod(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    number_op(lexp, rexp, |x, y| x % y)
}

fn number_op<F>(lexp: serde_json::Value, rexp: serde_json::Value, op: F) -> serde_json::Value
where
    F: FnOnce(u64, u64) -> u64,
{
    match (lexp, rexp) {
        (serde_json::Value::Number(n1), serde_json::Value::Number(n2)) => {
            serde_json::Value::Number(serde_json::Number::from(op(
                n1.as_u64().unwrap(),
                n2.as_u64().unwrap(),
            )))
        }
        (l, r) => panic!("Currently only uints are supported: {} {}", l, r),
    }
}

fn and(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    let b1 = coerce_to_bool(lexp);
    let b2 = coerce_to_bool(rexp);
    serde_json::Value::Bool(b1 && b2)
}

fn or(lexp: serde_json::Value, rexp: serde_json::Value) -> serde_json::Value {
    let b1 = coerce_to_bool(lexp);
    let b2 = coerce_to_bool(rexp);
    serde_json::Value::Bool(b1 || b2)
}

fn coerce_to_bool(exp: serde_json::Value) -> bool {
    match exp {
        serde_json::Value::Bool(b) => b,
        serde_json::Value::Number(i) => i.as_u64().unwrap() != 0,
        _ => panic!("Cannot coerce to bool: {}", exp),
    }
}

fn coerce_to_string(exp: serde_json::Value) -> String {
    match exp {
        serde_json::Value::String(s1) => s1,
        serde_json::Value::Number(s1) => s1.to_string(),
        _ => panic!("Cannot coerce to string: {}", exp),
    }
}

fn key_lookup(s: &str, json: &serde_json::Value) -> JsonataValue {
    match json {
        serde_json::Value::Array(arr) => {
            let arr = arr
                .iter()
                .filter_map(|obj| match obj {
                    serde_json::Value::Object(o) => Some(o.get(s).unwrap().clone()),
                    _ => None,
                })
                .collect();
            Some(serde_json::Value::Array(arr))
        }
        serde_json::Value::Object(obj) => Some(obj.get(s)?.clone()),
        _ => None,
    }
}

fn index_lookup(i: usize, json: &serde_json::Value) -> JsonataValue {
    match json {
        serde_json::Value::Array(arr) => Some(arr.get(i)?.clone()),
        _ => None,
    }
}

fn predicate_filter(key: &str, val: &str, json: &serde_json::Value) -> JsonataValue {
    match json {
        serde_json::Value::Array(arr) => {
            let arr = arr
                .iter()
                .filter_map(|obj| match obj {
                    serde_json::Value::Object(o) if o[key] == *val => {
                        Some(serde_json::Value::Object(o.clone()))
                    }
                    _ => None,
                })
                .collect();
            Some(serde_json::Value::Array(arr))
        }
        _ => None,
    }
}

fn orderby(key: &str, is_ascending: bool, json: &serde_json::Value) -> JsonataValue {
    match json {
        serde_json::Value::Array(arr) => {
            let mut arr = arr.clone();
            arr.sort_by(|a, b| match (a, b) {
                (serde_json::Value::Object(a), serde_json::Value::Object(b)) => {
                    match (&a[key], &b[key]) {
                        (serde_json::Value::String(a), serde_json::Value::String(b)) => a.cmp(b),
                        (serde_json::Value::Number(a), serde_json::Value::Number(b)) => a
                            .as_f64()
                            .unwrap()
                            .partial_cmp(&b.as_f64().unwrap())
                            .unwrap(),
                        _ => Ordering::Equal,
                    }
                }
                _ => Ordering::Equal,
            });
            if !is_ascending {
                arr.reverse();
            }
            Some(serde_json::Value::Array(arr))
        }
        _ => Some(json.clone()),
    }
}
