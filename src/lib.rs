//! Jsonata for Rust is an implementation of bringing a Jsonata like
//! API to the Rust language jsonata.org
//!
//! Author: Jeremy Hamilton 2022
#![allow(dead_code)]

extern crate pest;
#[macro_use]
extern crate pest_derive;

mod parser;
use parser::parse_json;
use parser::parse_jsonata;
use parser::AstNode;

/// The struct that holds the parsed Jsonata AST.
pub struct Jsonata {
    query: Vec<AstNode>,
}

impl Jsonata {
    /// Returns a Jsonata struct
    ///
    /// # Simple Queries
    ///
    /// You can work down a nested json object by providing the keys separated by `.`
    /// `Address.City`
    ///
    /// You can also navigate arrays by providing an index
    /// `Phone[1]`
    ///
    /// You can filter for specific values by providing a predicate
    /// `Phone[type='mobile'].number`
    ///
    /// If you have an array of data, you can sort the data by a particular key
    /// `Items^(Price)`
    ///
    /// You can also evaluate expressions by wrapping it in parentheses
    /// `Address.(Street & ', ' & City)`
    /// Currently, this library supports the types: String, uint, and bool
    /// The following operations are supported: `&` (concat), `+`, `-`, `/`, `*`, `%`, `and`, and `or`.
    ///
    /// # Arguments
    ///
    /// * `query` - the jsonata path
    ///
    pub fn new(query: &str) -> Jsonata {
        Jsonata {
            query: parse_jsonata(query)
                .unwrap_or_else(|_| panic!("Invalid jsonata query: \"{}\"", query)),
        }
    }

    /// Returns the serde_json::Value from the given Jsonata path.
    ///
    /// # Arguments
    ///
    /// * `json` - the json string to pull the data from.
    ///
    /// # Examples
    /// ```
    /// use jsonata::Jsonata;
    /// let jsonata = Jsonata::new("key");
    /// let val = jsonata.evaluate("{\"key\": 40}").unwrap();
    /// assert_eq!(40, val.as_u64().unwrap());
    /// ```
    pub fn evaluate(&self, json: &str) -> Option<serde_json::Value> {
        parse_json(&self.query, json)
    }
}

#[cfg(test)]
mod tests {
    use crate::Jsonata;

    const TEST_JSON: &str = r#"{
     "key": "value",
     "inner": {
         "second": "value2",
         "spaces and other chars?": "value3"
     },
     "index5": [
         "val0",
         "val1",
         "val2",
         "val3",
         "val4"
     ],
     "Phone": [
        {
        "type": "home",
        "number": "0203 544 1234"
        },
        {
        "type": "office",
        "number": "01962 001234"
        },
        {
        "type": "office",
        "number": "01962 001235"
        },
        {
        "type": "mobile",
        "number": "077 7700 1234"
        }
    ]  
    }"#;

    const TEST_JSON_ARRAY: &str = r#"[
        {"object0": "val0"},
        {"object1": "val1"},
        {"object2": "val2"},
        {"object3": "val3"},
        {"object4": "val4"},
        {"object5": "val5"}
    ]"#;

    const TEST_ORDERBY_JSON: &str = r#"{
        "Items":[
            {
                "Price": 40,
                "Quantity": 3
            },
            {
                "Price": 35.4,
                "Quantity": 6
            },
            {
                "Price": 56,
                "Quantity": 77
            },
            {
                "Price": -2,
                "Quantity": 4
            },
            {
                "Price": -2,
                "Quantity": 6
            }
    ]}"#;

    #[should_panic]
    #[test]
    fn new_jsonata() {
        Jsonata::new("");
    }

    #[test]
    fn eval_jsonata_simple_queries() {
        let json = Jsonata::new("key");
        let val = json.evaluate(TEST_JSON).unwrap();
        assert_eq!(serde_json::Value::String("value".to_string()), val);

        let json = Jsonata::new("inner.second");
        let val = json.evaluate(TEST_JSON).unwrap();
        assert_eq!(serde_json::Value::String("value2".to_string()), val);

        let json = Jsonata::new("inner.notReal");
        let val = json.evaluate(TEST_JSON);
        assert_eq!(None, val);

        let json = Jsonata::new("inner.`spaces and other chars?`");
        let val = json.evaluate(TEST_JSON).unwrap();
        assert_eq!(serde_json::Value::String("value3".to_string()), val);
    }

    #[test]
    fn eval_jsonata_root() {
        let json = Jsonata::new("$[0].object0");
        let val = json.evaluate(TEST_JSON_ARRAY).unwrap();
        assert_eq!(serde_json::Value::String("val0".to_string()), val);

        let json = Jsonata::new("$[5].object5");
        let val = json.evaluate(TEST_JSON_ARRAY).unwrap();
        assert_eq!(serde_json::Value::String("val5".to_string()), val);
    }

    #[test]
    fn eval_jsonata_indices() {
        let json = Jsonata::new("index5[0]");
        let val = json.evaluate(TEST_JSON).unwrap();
        assert_eq!(serde_json::Value::String("val0".to_string()), val);

        let json = Jsonata::new("index5[2]");
        let val = json.evaluate(TEST_JSON).unwrap();
        assert_eq!(serde_json::Value::String("val2".to_string()), val);

        let json = Jsonata::new("index4[5]");
        let val = json.evaluate(TEST_JSON);
        assert_eq!(None, val);
    }

    #[test]
    fn eval_jsonata_predicates() {
        let json = Jsonata::new("Phone[type='mobile']");
        let val_temp = json.evaluate(TEST_JSON).unwrap();
        let val = val_temp.as_array().unwrap()[0].as_object().unwrap();
        assert_eq!(
            "mobile".to_string(),
            val.get("type").unwrap().as_str().unwrap()
        );
        assert_eq!(
            "077 7700 1234".to_string(),
            val.get("number").unwrap().as_str().unwrap()
        );

        let json = Jsonata::new("Phone[type='mobile'].number");
        let val_temp = json.evaluate(TEST_JSON).unwrap();
        let val = &val_temp.as_array().unwrap()[0];
        assert_eq!("077 7700 1234".to_string(), val.as_str().unwrap());

        let json = Jsonata::new("Phone[type='office'].number");
        let val_temp = json.evaluate(TEST_JSON).unwrap();
        let val0 = &val_temp.as_array().unwrap()[0];
        let val1 = &val_temp.as_array().unwrap()[1];
        assert_eq!("01962 001234".to_string(), val0.as_str().unwrap());
        assert_eq!("01962 001235".to_string(), val1.as_str().unwrap());
    }

    #[test]
    fn eval_jsonata_orderby() {
        let json = Jsonata::new("Items^(Price)");
        let prices = json.evaluate(TEST_ORDERBY_JSON).unwrap();
        let val = prices.as_array().unwrap()[0].as_object().unwrap();
        assert_eq!(-2, val["Price"].as_i64().unwrap());

        let json = Jsonata::new("Items^(>Price)");
        let prices = json.evaluate(TEST_ORDERBY_JSON).unwrap();
        let val = prices.as_array().unwrap()[0].as_object().unwrap();
        assert_eq!(56, val["Price"].as_i64().unwrap());
    }

    #[test]
    fn test_concat() {
        let json = Jsonata::new("(key & notkey)");
        let concat_value = json.evaluate(TEST_JSON).unwrap();
        let val = concat_value.as_str().unwrap();
        assert_eq!("valuenotkey", val);

        let json = Jsonata::new("(key & ' but why?')");
        let concat_value = json.evaluate(TEST_JSON).unwrap();
        let val = concat_value.as_str().unwrap();
        assert_eq!("value but why?", val);

        let json = Jsonata::new("inner.(second & `spaces and other chars?`)");
        let concat_value = json.evaluate(TEST_JSON).unwrap();
        let val = concat_value.as_str().unwrap();
        assert_eq!("value2value3", val);
    }

    #[test]
    fn test_ints() {
        let json = Jsonata::new("(10 & 50)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!("1050", concat_value.as_str().unwrap());

        let json = Jsonata::new("(10 + 50)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!(60, concat_value.as_u64().unwrap());

        let json = Jsonata::new("(50 - 10)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!(40, concat_value.as_u64().unwrap());

        let json = Jsonata::new("(10 * 50)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!(500, concat_value.as_u64().unwrap());

        let json = Jsonata::new("(50 / 10)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!(5, concat_value.as_u64().unwrap());

        let json = Jsonata::new("(53 % 10)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!(3, concat_value.as_u64().unwrap());
    }

    #[test]
    fn test_bools() {
        let json = Jsonata::new("(true and false)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!(false, concat_value.as_bool().unwrap());

        let json = Jsonata::new("(true or false)");
        let concat_value = json.evaluate("{}").unwrap();
        assert_eq!(true, concat_value.as_bool().unwrap());
    }
}
